package com.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pizza365.model.COrder;

public interface OrderRepository extends JpaRepository<COrder, Long> {

}
