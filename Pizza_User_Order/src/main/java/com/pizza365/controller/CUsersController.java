package com.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.pizza365.model.*;
import com.pizza365.repository.*;

@RestController
public class CUsersController {
	@Autowired
	private UserRepository userRepository;

	@CrossOrigin
	@GetMapping("/users/all")
	public List<CUser> getAllUsers() {
		return userRepository.findAll();
	}

	@CrossOrigin
	@GetMapping("/users/details/{id}")
	public CUser getUserById(@PathVariable Long id) {
		if (userRepository.findById(id).isPresent())
			return userRepository.findById(id).get();
		else
			return null;
	}

	@CrossOrigin
	@PostMapping("/users/create")
	public ResponseEntity<Object> createUser(@RequestBody CUser cUser) {
		try {
			CUser newUser = new CUser();
			newUser.setFullName(cUser.getFullName());
			newUser.setAddress(cUser.getAddress());
			newUser.setEmail(cUser.getEmail());
			newUser.setPhone(cUser.getPhone());
			newUser.setOrders(cUser.getOrders());
			CUser savedUser = userRepository.save(newUser);
			return new ResponseEntity<>(savedUser, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/users/update/{id}")
	public ResponseEntity<Object> updateUser(@PathVariable("id") Long id, @RequestBody CUser cUser) {
		Optional<CUser> userData = userRepository.findById(id);
		if (userData.isPresent()) {
			CUser newUser = userData.get();
			newUser.setFullName(cUser.getFullName());
			newUser.setAddress(cUser.getAddress());
			newUser.setEmail(cUser.getEmail());
			newUser.setPhone(cUser.getPhone());
			newUser.setOrders(cUser.getOrders());
			CUser savedUser = userRepository.save(newUser);
			return new ResponseEntity<>(savedUser, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@CrossOrigin
	@DeleteMapping("/users/delete/{id}")
	public ResponseEntity<Object> deleteUserById(@PathVariable Long id) {
		try {
			Optional<CUser> optional= userRepository.findById(id);
			if (optional.isPresent()) {
				userRepository.deleteById(id);
			}else {
			}			
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
}
