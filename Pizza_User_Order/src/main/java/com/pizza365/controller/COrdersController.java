package com.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.pizza365.model.*;
import com.pizza365.repository.*;

@RestController
public class COrdersController {
	@Autowired
	private OrderRepository orderRepository;
	@Autowired
	private UserRepository userRepository;

	@CrossOrigin
	@GetMapping("/orders/all")
	public List<COrder> getAllRegion() {
		return orderRepository.findAll();
	}

	@CrossOrigin
	@GetMapping("/orders/userid/{id}")
	public List<COrder> getOrdersByUserId(@PathVariable Long id) {
		Optional<CUser> userOrder = userRepository.findById(id);
		if (userOrder.isPresent())
			return userOrder.get().getOrders();
		else
			return null;
	}

	@CrossOrigin
	@GetMapping("/orders/details/{id}")
	public COrder getOrderById(@PathVariable Long id) {
		if (orderRepository.findById(id).isPresent())
			return orderRepository.findById(id).get();
		else
			return null;
	}

	@CrossOrigin
	@PostMapping("/orders/create/{id}")
	public ResponseEntity<Object> createOrder(@PathVariable("id") Long id, @RequestBody COrder cOrder) {
		Optional<CUser> userData = userRepository.findById(id);
		if (userData.isPresent())
			try {
				COrder newOrder = new COrder();
				newOrder.setOrderCode(cOrder.getOrderCode());
				newOrder.setPaid(cOrder.getPaid());
				newOrder.setPizzaSize(cOrder.getPizzaSize());
				newOrder.setPizzaType(cOrder.getPizzaType());
				newOrder.setPrice(cOrder.getPrice());
				newOrder.setVoucherCode(cOrder.getVoucherCode());
				newOrder.setUser(userData.get());
				COrder savedOrder = orderRepository.save(newOrder);
				return new ResponseEntity<>(savedOrder, HttpStatus.CREATED);
			} catch (Exception e) {
				System.out.println(e);
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		else
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}

	@CrossOrigin
	@PutMapping("/orders/update/{id}")
	public ResponseEntity<Object> updateOrder(@PathVariable("id") Long id, @RequestBody COrder cOrder) {
		Optional<COrder> orderData = orderRepository.findById(id);
		if (orderData.isPresent()) {
			COrder newOrder = orderData.get();
			newOrder.setOrderCode(cOrder.getOrderCode());
			newOrder.setPaid(cOrder.getPaid());
			newOrder.setPizzaSize(cOrder.getPizzaSize());
			newOrder.setPizzaType(cOrder.getPizzaType());
			newOrder.setPrice(cOrder.getPrice());
			newOrder.setVoucherCode(cOrder.getVoucherCode());
			COrder savedOrder = orderRepository.save(newOrder);
			return new ResponseEntity<>(savedOrder, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/orders/delete/{id}")
	public ResponseEntity<Object> deleteOrderById(@PathVariable Long id) {
		try {
			orderRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
