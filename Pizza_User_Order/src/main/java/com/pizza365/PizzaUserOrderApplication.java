package com.pizza365;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PizzaUserOrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(PizzaUserOrderApplication.class, args);
	}

}
